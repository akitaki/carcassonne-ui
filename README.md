# Carcassonne UI

![](./screenshot.png)

Gameplay UI for [Carcassonne](<https://en.wikipedia.org/wiki/Carcassonne_(board_game)>) written in Flutter.

See the blog posts [part I](https://akitaki.gitlab.io/carcassonne-ai/) and [part II](https://akitaki.gitlab.io/carcassonne-ai-part-2/)
for more information.
