///
//  Generated code. Do not modify.
//  source: carcassonne.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'carcassonne.pbenum.dart';

export 'carcassonne.pbenum.dart';

class CreateRoomRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'CreateRoomRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'carcassonne'), createEmptyInstance: create)
    ..aOB(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'withNpc', protoName: 'withNpc')
    ..hasRequiredFields = false
  ;

  CreateRoomRequest._() : super();
  factory CreateRoomRequest({
    $core.bool? withNpc,
  }) {
    final _result = create();
    if (withNpc != null) {
      _result.withNpc = withNpc;
    }
    return _result;
  }
  factory CreateRoomRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory CreateRoomRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  CreateRoomRequest clone() => CreateRoomRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  CreateRoomRequest copyWith(void Function(CreateRoomRequest) updates) => super.copyWith((message) => updates(message as CreateRoomRequest)) as CreateRoomRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CreateRoomRequest create() => CreateRoomRequest._();
  CreateRoomRequest createEmptyInstance() => create();
  static $pb.PbList<CreateRoomRequest> createRepeated() => $pb.PbList<CreateRoomRequest>();
  @$core.pragma('dart2js:noInline')
  static CreateRoomRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<CreateRoomRequest>(create);
  static CreateRoomRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.bool get withNpc => $_getBF(0);
  @$pb.TagNumber(1)
  set withNpc($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasWithNpc() => $_has(0);
  @$pb.TagNumber(1)
  void clearWithNpc() => clearField(1);
}

class CreateRoomResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'CreateRoomResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'carcassonne'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'roomId', $pb.PbFieldType.OU3, protoName: 'roomId')
    ..hasRequiredFields = false
  ;

  CreateRoomResponse._() : super();
  factory CreateRoomResponse({
    $core.int? roomId,
  }) {
    final _result = create();
    if (roomId != null) {
      _result.roomId = roomId;
    }
    return _result;
  }
  factory CreateRoomResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory CreateRoomResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  CreateRoomResponse clone() => CreateRoomResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  CreateRoomResponse copyWith(void Function(CreateRoomResponse) updates) => super.copyWith((message) => updates(message as CreateRoomResponse)) as CreateRoomResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CreateRoomResponse create() => CreateRoomResponse._();
  CreateRoomResponse createEmptyInstance() => create();
  static $pb.PbList<CreateRoomResponse> createRepeated() => $pb.PbList<CreateRoomResponse>();
  @$core.pragma('dart2js:noInline')
  static CreateRoomResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<CreateRoomResponse>(create);
  static CreateRoomResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get roomId => $_getIZ(0);
  @$pb.TagNumber(1)
  set roomId($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasRoomId() => $_has(0);
  @$pb.TagNumber(1)
  void clearRoomId() => clearField(1);
}

class JoinRoomRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'JoinRoomRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'carcassonne'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'roomId', $pb.PbFieldType.OU3, protoName: 'roomId')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..hasRequiredFields = false
  ;

  JoinRoomRequest._() : super();
  factory JoinRoomRequest({
    $core.int? roomId,
    $core.String? name,
  }) {
    final _result = create();
    if (roomId != null) {
      _result.roomId = roomId;
    }
    if (name != null) {
      _result.name = name;
    }
    return _result;
  }
  factory JoinRoomRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory JoinRoomRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  JoinRoomRequest clone() => JoinRoomRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  JoinRoomRequest copyWith(void Function(JoinRoomRequest) updates) => super.copyWith((message) => updates(message as JoinRoomRequest)) as JoinRoomRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static JoinRoomRequest create() => JoinRoomRequest._();
  JoinRoomRequest createEmptyInstance() => create();
  static $pb.PbList<JoinRoomRequest> createRepeated() => $pb.PbList<JoinRoomRequest>();
  @$core.pragma('dart2js:noInline')
  static JoinRoomRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<JoinRoomRequest>(create);
  static JoinRoomRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get roomId => $_getIZ(0);
  @$pb.TagNumber(1)
  set roomId($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasRoomId() => $_has(0);
  @$pb.TagNumber(1)
  void clearRoomId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get name => $_getSZ(1);
  @$pb.TagNumber(2)
  set name($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasName() => $_has(1);
  @$pb.TagNumber(2)
  void clearName() => clearField(2);
}

class JoinRoomResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'JoinRoomResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'carcassonne'), createEmptyInstance: create)
    ..e<Player>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'me', $pb.PbFieldType.OE, defaultOrMaker: Player.Red, valueOf: Player.valueOf, enumValues: Player.values)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'token')
    ..hasRequiredFields = false
  ;

  JoinRoomResponse._() : super();
  factory JoinRoomResponse({
    Player? me,
    $core.String? token,
  }) {
    final _result = create();
    if (me != null) {
      _result.me = me;
    }
    if (token != null) {
      _result.token = token;
    }
    return _result;
  }
  factory JoinRoomResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory JoinRoomResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  JoinRoomResponse clone() => JoinRoomResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  JoinRoomResponse copyWith(void Function(JoinRoomResponse) updates) => super.copyWith((message) => updates(message as JoinRoomResponse)) as JoinRoomResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static JoinRoomResponse create() => JoinRoomResponse._();
  JoinRoomResponse createEmptyInstance() => create();
  static $pb.PbList<JoinRoomResponse> createRepeated() => $pb.PbList<JoinRoomResponse>();
  @$core.pragma('dart2js:noInline')
  static JoinRoomResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<JoinRoomResponse>(create);
  static JoinRoomResponse? _defaultInstance;

  @$pb.TagNumber(1)
  Player get me => $_getN(0);
  @$pb.TagNumber(1)
  set me(Player v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasMe() => $_has(0);
  @$pb.TagNumber(1)
  void clearMe() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get token => $_getSZ(1);
  @$pb.TagNumber(2)
  set token($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasToken() => $_has(1);
  @$pb.TagNumber(2)
  void clearToken() => clearField(2);
}

class GameStateRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GameStateRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'carcassonne'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'roomId', $pb.PbFieldType.OU3, protoName: 'roomId')
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'lastSerial', $pb.PbFieldType.OU3, protoName: 'lastSerial')
    ..hasRequiredFields = false
  ;

  GameStateRequest._() : super();
  factory GameStateRequest({
    $core.int? roomId,
    $core.int? lastSerial,
  }) {
    final _result = create();
    if (roomId != null) {
      _result.roomId = roomId;
    }
    if (lastSerial != null) {
      _result.lastSerial = lastSerial;
    }
    return _result;
  }
  factory GameStateRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GameStateRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GameStateRequest clone() => GameStateRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GameStateRequest copyWith(void Function(GameStateRequest) updates) => super.copyWith((message) => updates(message as GameStateRequest)) as GameStateRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GameStateRequest create() => GameStateRequest._();
  GameStateRequest createEmptyInstance() => create();
  static $pb.PbList<GameStateRequest> createRepeated() => $pb.PbList<GameStateRequest>();
  @$core.pragma('dart2js:noInline')
  static GameStateRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GameStateRequest>(create);
  static GameStateRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get roomId => $_getIZ(0);
  @$pb.TagNumber(1)
  set roomId($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasRoomId() => $_has(0);
  @$pb.TagNumber(1)
  void clearRoomId() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get lastSerial => $_getIZ(1);
  @$pb.TagNumber(2)
  set lastSerial($core.int v) { $_setUnsignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasLastSerial() => $_has(1);
  @$pb.TagNumber(2)
  void clearLastSerial() => clearField(2);
}

class GameStateResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GameStateResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'carcassonne'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'serial', $pb.PbFieldType.OU3)
    ..aOB(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'started')
    ..aOM<Board>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'board', subBuilder: Board.create)
    ..a<$core.int>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'numPlayers', $pb.PbFieldType.OU3, protoName: 'numPlayers')
    ..pc<Action>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'legalActions', $pb.PbFieldType.PM, protoName: 'legalActions', subBuilder: Action.create)
    ..pPS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'playerNames', protoName: 'playerNames')
    ..hasRequiredFields = false
  ;

  GameStateResponse._() : super();
  factory GameStateResponse({
    $core.int? serial,
    $core.bool? started,
    Board? board,
    $core.int? numPlayers,
    $core.Iterable<Action>? legalActions,
    $core.Iterable<$core.String>? playerNames,
  }) {
    final _result = create();
    if (serial != null) {
      _result.serial = serial;
    }
    if (started != null) {
      _result.started = started;
    }
    if (board != null) {
      _result.board = board;
    }
    if (numPlayers != null) {
      _result.numPlayers = numPlayers;
    }
    if (legalActions != null) {
      _result.legalActions.addAll(legalActions);
    }
    if (playerNames != null) {
      _result.playerNames.addAll(playerNames);
    }
    return _result;
  }
  factory GameStateResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GameStateResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GameStateResponse clone() => GameStateResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GameStateResponse copyWith(void Function(GameStateResponse) updates) => super.copyWith((message) => updates(message as GameStateResponse)) as GameStateResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GameStateResponse create() => GameStateResponse._();
  GameStateResponse createEmptyInstance() => create();
  static $pb.PbList<GameStateResponse> createRepeated() => $pb.PbList<GameStateResponse>();
  @$core.pragma('dart2js:noInline')
  static GameStateResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GameStateResponse>(create);
  static GameStateResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get serial => $_getIZ(0);
  @$pb.TagNumber(1)
  set serial($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasSerial() => $_has(0);
  @$pb.TagNumber(1)
  void clearSerial() => clearField(1);

  @$pb.TagNumber(2)
  $core.bool get started => $_getBF(1);
  @$pb.TagNumber(2)
  set started($core.bool v) { $_setBool(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasStarted() => $_has(1);
  @$pb.TagNumber(2)
  void clearStarted() => clearField(2);

  @$pb.TagNumber(3)
  Board get board => $_getN(2);
  @$pb.TagNumber(3)
  set board(Board v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasBoard() => $_has(2);
  @$pb.TagNumber(3)
  void clearBoard() => clearField(3);
  @$pb.TagNumber(3)
  Board ensureBoard() => $_ensure(2);

  @$pb.TagNumber(4)
  $core.int get numPlayers => $_getIZ(3);
  @$pb.TagNumber(4)
  set numPlayers($core.int v) { $_setUnsignedInt32(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasNumPlayers() => $_has(3);
  @$pb.TagNumber(4)
  void clearNumPlayers() => clearField(4);

  @$pb.TagNumber(5)
  $core.List<Action> get legalActions => $_getList(4);

  @$pb.TagNumber(6)
  $core.List<$core.String> get playerNames => $_getList(5);
}

class DoActionRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'DoActionRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'carcassonne'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'roomId', $pb.PbFieldType.OU3, protoName: 'roomId')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'token')
    ..aOM<Action>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'action', subBuilder: Action.create)
    ..hasRequiredFields = false
  ;

  DoActionRequest._() : super();
  factory DoActionRequest({
    $core.int? roomId,
    $core.String? token,
    Action? action,
  }) {
    final _result = create();
    if (roomId != null) {
      _result.roomId = roomId;
    }
    if (token != null) {
      _result.token = token;
    }
    if (action != null) {
      _result.action = action;
    }
    return _result;
  }
  factory DoActionRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DoActionRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  DoActionRequest clone() => DoActionRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  DoActionRequest copyWith(void Function(DoActionRequest) updates) => super.copyWith((message) => updates(message as DoActionRequest)) as DoActionRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DoActionRequest create() => DoActionRequest._();
  DoActionRequest createEmptyInstance() => create();
  static $pb.PbList<DoActionRequest> createRepeated() => $pb.PbList<DoActionRequest>();
  @$core.pragma('dart2js:noInline')
  static DoActionRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DoActionRequest>(create);
  static DoActionRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get roomId => $_getIZ(0);
  @$pb.TagNumber(1)
  set roomId($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasRoomId() => $_has(0);
  @$pb.TagNumber(1)
  void clearRoomId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get token => $_getSZ(1);
  @$pb.TagNumber(2)
  set token($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasToken() => $_has(1);
  @$pb.TagNumber(2)
  void clearToken() => clearField(2);

  @$pb.TagNumber(3)
  Action get action => $_getN(2);
  @$pb.TagNumber(3)
  set action(Action v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasAction() => $_has(2);
  @$pb.TagNumber(3)
  void clearAction() => clearField(3);
  @$pb.TagNumber(3)
  Action ensureAction() => $_ensure(2);
}

class Empty extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Empty', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'carcassonne'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  Empty._() : super();
  factory Empty() => create();
  factory Empty.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Empty.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Empty clone() => Empty()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Empty copyWith(void Function(Empty) updates) => super.copyWith((message) => updates(message as Empty)) as Empty; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Empty create() => Empty._();
  Empty createEmptyInstance() => create();
  static $pb.PbList<Empty> createRepeated() => $pb.PbList<Empty>();
  @$core.pragma('dart2js:noInline')
  static Empty getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Empty>(create);
  static Empty? _defaultInstance;
}

class Board extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Board', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'carcassonne'), createEmptyInstance: create)
    ..pc<TileEntry>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'tiles', $pb.PbFieldType.PM, subBuilder: TileEntry.create)
    ..pc<TileKind>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'deck', $pb.PbFieldType.PE, valueOf: TileKind.valueOf, enumValues: TileKind.values)
    ..e<Player>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'turn', $pb.PbFieldType.OE, defaultOrMaker: Player.Red, valueOf: Player.valueOf, enumValues: Player.values)
    ..p<$core.int>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'scores', $pb.PbFieldType.PU3)
    ..p<$core.int>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'remainingMeeples', $pb.PbFieldType.PU3, protoName: 'remainingMeeples')
    ..hasRequiredFields = false
  ;

  Board._() : super();
  factory Board({
    $core.Iterable<TileEntry>? tiles,
    $core.Iterable<TileKind>? deck,
    Player? turn,
    $core.Iterable<$core.int>? scores,
    $core.Iterable<$core.int>? remainingMeeples,
  }) {
    final _result = create();
    if (tiles != null) {
      _result.tiles.addAll(tiles);
    }
    if (deck != null) {
      _result.deck.addAll(deck);
    }
    if (turn != null) {
      _result.turn = turn;
    }
    if (scores != null) {
      _result.scores.addAll(scores);
    }
    if (remainingMeeples != null) {
      _result.remainingMeeples.addAll(remainingMeeples);
    }
    return _result;
  }
  factory Board.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Board.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Board clone() => Board()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Board copyWith(void Function(Board) updates) => super.copyWith((message) => updates(message as Board)) as Board; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Board create() => Board._();
  Board createEmptyInstance() => create();
  static $pb.PbList<Board> createRepeated() => $pb.PbList<Board>();
  @$core.pragma('dart2js:noInline')
  static Board getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Board>(create);
  static Board? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<TileEntry> get tiles => $_getList(0);

  @$pb.TagNumber(2)
  $core.List<TileKind> get deck => $_getList(1);

  @$pb.TagNumber(3)
  Player get turn => $_getN(2);
  @$pb.TagNumber(3)
  set turn(Player v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasTurn() => $_has(2);
  @$pb.TagNumber(3)
  void clearTurn() => clearField(3);

  @$pb.TagNumber(4)
  $core.List<$core.int> get scores => $_getList(3);

  @$pb.TagNumber(5)
  $core.List<$core.int> get remainingMeeples => $_getList(4);
}

class TileEntry extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'TileEntry', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'carcassonne'), createEmptyInstance: create)
    ..aOM<Coord>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'coord', subBuilder: Coord.create)
    ..aOM<Tile>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'tile', subBuilder: Tile.create)
    ..aOM<Meeple>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'meeple', subBuilder: Meeple.create)
    ..hasRequiredFields = false
  ;

  TileEntry._() : super();
  factory TileEntry({
    Coord? coord,
    Tile? tile,
    Meeple? meeple,
  }) {
    final _result = create();
    if (coord != null) {
      _result.coord = coord;
    }
    if (tile != null) {
      _result.tile = tile;
    }
    if (meeple != null) {
      _result.meeple = meeple;
    }
    return _result;
  }
  factory TileEntry.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TileEntry.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  TileEntry clone() => TileEntry()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  TileEntry copyWith(void Function(TileEntry) updates) => super.copyWith((message) => updates(message as TileEntry)) as TileEntry; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TileEntry create() => TileEntry._();
  TileEntry createEmptyInstance() => create();
  static $pb.PbList<TileEntry> createRepeated() => $pb.PbList<TileEntry>();
  @$core.pragma('dart2js:noInline')
  static TileEntry getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TileEntry>(create);
  static TileEntry? _defaultInstance;

  @$pb.TagNumber(1)
  Coord get coord => $_getN(0);
  @$pb.TagNumber(1)
  set coord(Coord v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasCoord() => $_has(0);
  @$pb.TagNumber(1)
  void clearCoord() => clearField(1);
  @$pb.TagNumber(1)
  Coord ensureCoord() => $_ensure(0);

  @$pb.TagNumber(2)
  Tile get tile => $_getN(1);
  @$pb.TagNumber(2)
  set tile(Tile v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasTile() => $_has(1);
  @$pb.TagNumber(2)
  void clearTile() => clearField(2);
  @$pb.TagNumber(2)
  Tile ensureTile() => $_ensure(1);

  @$pb.TagNumber(3)
  Meeple get meeple => $_getN(2);
  @$pb.TagNumber(3)
  set meeple(Meeple v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasMeeple() => $_has(2);
  @$pb.TagNumber(3)
  void clearMeeple() => clearField(3);
  @$pb.TagNumber(3)
  Meeple ensureMeeple() => $_ensure(2);
}

class Coord extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Coord', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'carcassonne'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'i', $pb.PbFieldType.O3)
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'j', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  Coord._() : super();
  factory Coord({
    $core.int? i,
    $core.int? j,
  }) {
    final _result = create();
    if (i != null) {
      _result.i = i;
    }
    if (j != null) {
      _result.j = j;
    }
    return _result;
  }
  factory Coord.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Coord.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Coord clone() => Coord()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Coord copyWith(void Function(Coord) updates) => super.copyWith((message) => updates(message as Coord)) as Coord; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Coord create() => Coord._();
  Coord createEmptyInstance() => create();
  static $pb.PbList<Coord> createRepeated() => $pb.PbList<Coord>();
  @$core.pragma('dart2js:noInline')
  static Coord getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Coord>(create);
  static Coord? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get i => $_getIZ(0);
  @$pb.TagNumber(1)
  set i($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasI() => $_has(0);
  @$pb.TagNumber(1)
  void clearI() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get j => $_getIZ(1);
  @$pb.TagNumber(2)
  set j($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasJ() => $_has(1);
  @$pb.TagNumber(2)
  void clearJ() => clearField(2);
}

class Tile extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Tile', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'carcassonne'), createEmptyInstance: create)
    ..e<TileKind>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'kind', $pb.PbFieldType.OE, defaultOrMaker: TileKind.CastleCenterPennant, valueOf: TileKind.valueOf, enumValues: TileKind.values)
    ..e<Rotation>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'rotation', $pb.PbFieldType.OE, defaultOrMaker: Rotation.R0, valueOf: Rotation.valueOf, enumValues: Rotation.values)
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'meepleOnFeature', $pb.PbFieldType.OU3)
    ..a<$core.int>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'meepleOnCloister', $pb.PbFieldType.OU3)
    ..hasRequiredFields = false
  ;

  Tile._() : super();
  factory Tile({
    TileKind? kind,
    Rotation? rotation,
    $core.int? meepleOnFeature,
    $core.int? meepleOnCloister,
  }) {
    final _result = create();
    if (kind != null) {
      _result.kind = kind;
    }
    if (rotation != null) {
      _result.rotation = rotation;
    }
    if (meepleOnFeature != null) {
      _result.meepleOnFeature = meepleOnFeature;
    }
    if (meepleOnCloister != null) {
      _result.meepleOnCloister = meepleOnCloister;
    }
    return _result;
  }
  factory Tile.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Tile.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Tile clone() => Tile()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Tile copyWith(void Function(Tile) updates) => super.copyWith((message) => updates(message as Tile)) as Tile; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Tile create() => Tile._();
  Tile createEmptyInstance() => create();
  static $pb.PbList<Tile> createRepeated() => $pb.PbList<Tile>();
  @$core.pragma('dart2js:noInline')
  static Tile getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Tile>(create);
  static Tile? _defaultInstance;

  @$pb.TagNumber(1)
  TileKind get kind => $_getN(0);
  @$pb.TagNumber(1)
  set kind(TileKind v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasKind() => $_has(0);
  @$pb.TagNumber(1)
  void clearKind() => clearField(1);

  @$pb.TagNumber(2)
  Rotation get rotation => $_getN(1);
  @$pb.TagNumber(2)
  set rotation(Rotation v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasRotation() => $_has(1);
  @$pb.TagNumber(2)
  void clearRotation() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get meepleOnFeature => $_getIZ(2);
  @$pb.TagNumber(3)
  set meepleOnFeature($core.int v) { $_setUnsignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasMeepleOnFeature() => $_has(2);
  @$pb.TagNumber(3)
  void clearMeepleOnFeature() => clearField(3);

  @$pb.TagNumber(4)
  $core.int get meepleOnCloister => $_getIZ(3);
  @$pb.TagNumber(4)
  set meepleOnCloister($core.int v) { $_setUnsignedInt32(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasMeepleOnCloister() => $_has(3);
  @$pb.TagNumber(4)
  void clearMeepleOnCloister() => clearField(4);
}

class Action extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Action', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'carcassonne'), createEmptyInstance: create)
    ..aOM<Coord>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'coord', subBuilder: Coord.create)
    ..e<Rotation>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'rotation', $pb.PbFieldType.OE, defaultOrMaker: Rotation.R0, valueOf: Rotation.valueOf, enumValues: Rotation.values)
    ..aOM<MeepleAction>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'meepleAction', subBuilder: MeepleAction.create)
    ..hasRequiredFields = false
  ;

  Action._() : super();
  factory Action({
    Coord? coord,
    Rotation? rotation,
    MeepleAction? meepleAction,
  }) {
    final _result = create();
    if (coord != null) {
      _result.coord = coord;
    }
    if (rotation != null) {
      _result.rotation = rotation;
    }
    if (meepleAction != null) {
      _result.meepleAction = meepleAction;
    }
    return _result;
  }
  factory Action.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Action.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Action clone() => Action()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Action copyWith(void Function(Action) updates) => super.copyWith((message) => updates(message as Action)) as Action; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Action create() => Action._();
  Action createEmptyInstance() => create();
  static $pb.PbList<Action> createRepeated() => $pb.PbList<Action>();
  @$core.pragma('dart2js:noInline')
  static Action getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Action>(create);
  static Action? _defaultInstance;

  @$pb.TagNumber(1)
  Coord get coord => $_getN(0);
  @$pb.TagNumber(1)
  set coord(Coord v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasCoord() => $_has(0);
  @$pb.TagNumber(1)
  void clearCoord() => clearField(1);
  @$pb.TagNumber(1)
  Coord ensureCoord() => $_ensure(0);

  @$pb.TagNumber(2)
  Rotation get rotation => $_getN(1);
  @$pb.TagNumber(2)
  set rotation(Rotation v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasRotation() => $_has(1);
  @$pb.TagNumber(2)
  void clearRotation() => clearField(2);

  @$pb.TagNumber(3)
  MeepleAction get meepleAction => $_getN(2);
  @$pb.TagNumber(3)
  set meepleAction(MeepleAction v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasMeepleAction() => $_has(2);
  @$pb.TagNumber(3)
  void clearMeepleAction() => clearField(3);
  @$pb.TagNumber(3)
  MeepleAction ensureMeepleAction() => $_ensure(2);
}

enum MeepleAction_MeepleAction {
  none, 
  cloister, 
  edge, 
  notSet
}

class MeepleAction extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, MeepleAction_MeepleAction> _MeepleAction_MeepleActionByTag = {
    1 : MeepleAction_MeepleAction.none,
    2 : MeepleAction_MeepleAction.cloister,
    3 : MeepleAction_MeepleAction.edge,
    0 : MeepleAction_MeepleAction.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MeepleAction', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'carcassonne'), createEmptyInstance: create)
    ..oo(0, [1, 2, 3])
    ..aOB(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'none')
    ..aOB(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'cloister')
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'edge', $pb.PbFieldType.OU3)
    ..hasRequiredFields = false
  ;

  MeepleAction._() : super();
  factory MeepleAction({
    $core.bool? none,
    $core.bool? cloister,
    $core.int? edge,
  }) {
    final _result = create();
    if (none != null) {
      _result.none = none;
    }
    if (cloister != null) {
      _result.cloister = cloister;
    }
    if (edge != null) {
      _result.edge = edge;
    }
    return _result;
  }
  factory MeepleAction.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MeepleAction.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MeepleAction clone() => MeepleAction()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MeepleAction copyWith(void Function(MeepleAction) updates) => super.copyWith((message) => updates(message as MeepleAction)) as MeepleAction; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MeepleAction create() => MeepleAction._();
  MeepleAction createEmptyInstance() => create();
  static $pb.PbList<MeepleAction> createRepeated() => $pb.PbList<MeepleAction>();
  @$core.pragma('dart2js:noInline')
  static MeepleAction getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MeepleAction>(create);
  static MeepleAction? _defaultInstance;

  MeepleAction_MeepleAction whichMeepleAction() => _MeepleAction_MeepleActionByTag[$_whichOneof(0)]!;
  void clearMeepleAction() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $core.bool get none => $_getBF(0);
  @$pb.TagNumber(1)
  set none($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasNone() => $_has(0);
  @$pb.TagNumber(1)
  void clearNone() => clearField(1);

  @$pb.TagNumber(2)
  $core.bool get cloister => $_getBF(1);
  @$pb.TagNumber(2)
  set cloister($core.bool v) { $_setBool(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasCloister() => $_has(1);
  @$pb.TagNumber(2)
  void clearCloister() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get edge => $_getIZ(2);
  @$pb.TagNumber(3)
  set edge($core.int v) { $_setUnsignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasEdge() => $_has(2);
  @$pb.TagNumber(3)
  void clearEdge() => clearField(3);
}

class Meeple extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Meeple', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'carcassonne'), createEmptyInstance: create)
    ..aOM<MeepleAction>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'meepleAction', subBuilder: MeepleAction.create)
    ..e<Player>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'player', $pb.PbFieldType.OE, defaultOrMaker: Player.Red, valueOf: Player.valueOf, enumValues: Player.values)
    ..hasRequiredFields = false
  ;

  Meeple._() : super();
  factory Meeple({
    MeepleAction? meepleAction,
    Player? player,
  }) {
    final _result = create();
    if (meepleAction != null) {
      _result.meepleAction = meepleAction;
    }
    if (player != null) {
      _result.player = player;
    }
    return _result;
  }
  factory Meeple.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Meeple.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Meeple clone() => Meeple()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Meeple copyWith(void Function(Meeple) updates) => super.copyWith((message) => updates(message as Meeple)) as Meeple; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Meeple create() => Meeple._();
  Meeple createEmptyInstance() => create();
  static $pb.PbList<Meeple> createRepeated() => $pb.PbList<Meeple>();
  @$core.pragma('dart2js:noInline')
  static Meeple getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Meeple>(create);
  static Meeple? _defaultInstance;

  @$pb.TagNumber(1)
  MeepleAction get meepleAction => $_getN(0);
  @$pb.TagNumber(1)
  set meepleAction(MeepleAction v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasMeepleAction() => $_has(0);
  @$pb.TagNumber(1)
  void clearMeepleAction() => clearField(1);
  @$pb.TagNumber(1)
  MeepleAction ensureMeepleAction() => $_ensure(0);

  @$pb.TagNumber(2)
  Player get player => $_getN(1);
  @$pb.TagNumber(2)
  set player(Player v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasPlayer() => $_has(1);
  @$pb.TagNumber(2)
  void clearPlayer() => clearField(2);
}

