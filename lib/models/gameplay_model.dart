import 'package:flutter/foundation.dart';
import 'package:grpc/grpc.dart';
import 'package:grpc/grpc_or_grpcweb.dart';

import '../generated/carcassonne.pb.dart';
import '../generated/carcassonne.pbgrpc.dart';
import './endpoint.dart';

typedef ConnectionErrorCallback = void Function({
  required String message,
  required GameplayModel model,
  required bool disconnected,
});

class GameplayModel extends ChangeNotifier {
  final GrpcOrGrpcWebClientChannel channel;
  final GameServiceClient client;
  final int roomId;
  final String token;
  final Player me;
  final void Function()? onGameStart;
  final ConnectionErrorCallback? onConnectionError;

  bool _stopped = false;

  GameStateResponse? _state;
  GameStateResponse? get state => this._state;

  bool get isMyTurn => this._state?.board.turn == me;

  Coord? _selectedCoord;
  Coord? get selectedCoord => this._selectedCoord;
  void setSelectedCoord(Coord coord) {
    this._selectedCoord = coord;
    notifyListeners();
  }

  void unsetSelectedCoord() {
    this._selectedCoord = null;
    notifyListeners();
  }

  List<int>? get scores {
    final maybeScores = this._state?.board.scores;
    if (maybeScores != null && maybeScores.isNotEmpty) {
      return maybeScores;
    } else {
      return null;
    }
  }

  List<int>? get meeplesRemaining {
    final maybeMeeplesRemaining = this._state?.board.remainingMeeples;
    if (maybeMeeplesRemaining != null && maybeMeeplesRemaining.isNotEmpty) {
      return maybeMeeplesRemaining;
    } else {
      return null;
    }
  }

  @Deprecated('Use callback instead')
  bool get started => this._state?.started ?? false;

  GameplayModel({
    required this.channel,
    required this.client,
    required this.roomId,
    required this.me,
    required this.token,
    this.onGameStart,
    this.onConnectionError,
  });

  static GameplayModel makeDefault({
    required int roomId,
    required Player me,
    required String token,
    void Function()? onGameStart,
    ConnectionErrorCallback? onConnectionError,
  }) {
    final channel = GrpcOrGrpcWebClientChannel.toSingleEndpoint(
      host: endpointUrl(),
      port: endpointPort(),
      transportSecure: false,
    );
    final client = GameServiceClient(channel);

    final model = GameplayModel(
      channel: channel,
      client: client,
      roomId: roomId,
      token: token,
      me: me,
      onGameStart: onGameStart,
      onConnectionError: onConnectionError,
    );
    model.startStateLoop();

    return model;
  }

  /// The main state-fetching loop
  Future<void> startStateLoop() async {
    _state = null;

    try {
      await _waitUntilGameStart();
      print('game started');
      this.onGameStart?.call();
      notifyListeners();

      while (this._state!.board.deck.isNotEmpty) {
        this._state = await this.client.gameState(
              GameStateRequest(
                roomId: roomId,
                lastSerial: this._state!.serial,
              ),
            );
        // _logState();
        _selectedCoord = null;
        notifyListeners();
      }
      print('game ended');
    } on GrpcError catch (error) {
      // don't report error if it's already stopped
      if (_stopped) return;

      this.onConnectionError?.call(
            message: error.message ?? '$error',
            model: this,
            disconnected: true,
          );
      notifyListeners();
    }
  }

  void stopStateLoop() {
    _stopped = true;
    this.channel.shutdown();
  }

  Future<void> doAction(Action action) async {
    print('doing action: ${action.writeToJson()}');
    try {
      await this.client.doAction(DoActionRequest(
            roomId: this.roomId,
            token: this.token,
            action: action,
          ));
      notifyListeners();
    } on GrpcError catch (error) {
      // don't report error if it's already stopped
      if (_stopped) return;

      this.onConnectionError?.call(
            message: error.message ?? '$error',
            model: this,
            disconnected: false,
          );
      notifyListeners();
    }
  }

  /// Modifies [_state].
  Future<void> _waitUntilGameStart() async {
    do {
      this._state = await this.client.gameState(
            GameStateRequest(
              roomId: roomId,
              lastSerial: _state?.serial ?? 0,
            ),
          );
      notifyListeners();
    } while (this._state!.started == false);
  }
}
