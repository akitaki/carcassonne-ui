import 'package:flutter/foundation.dart';
import 'package:grpc/grpc_or_grpcweb.dart';

import '../generated/carcassonne.pb.dart';
import '../generated/carcassonne.pbgrpc.dart';
import './endpoint.dart';

class RoomModel extends ChangeNotifier {
  final GrpcOrGrpcWebClientChannel channel;
  final GameServiceClient client;

  int? _roomId;

  RoomModel({required this.channel, required this.client});
  static RoomModel makeDefault() {
    final channel = GrpcOrGrpcWebClientChannel.toSingleEndpoint(
      host: endpointUrl(),
      port: endpointPort(),
      transportSecure: false,
    );
    final client = GameServiceClient(channel);

    return RoomModel(channel: channel, client: client);
  }

  int? get roomId {
    return _roomId;
  }

  /// Returns the newly created room id
  Future<JoinRoomResult> createAndJoinRoom({
    required String nickname,
    required bool singlePlayer,
  }) async {
    final roomId = (await client.createRoom(CreateRoomRequest(
      withNpc: singlePlayer,
    )))
        .roomId;
    final res = await _joinRoom(roomId, nickname);
    _roomId = roomId;

    notifyListeners();
    return JoinRoomResult(roomId: roomId, me: res.me, token: res.token);
  }

  Future<JoinRoomResult> joinRoom(int roomId, String nickname) async {
    final res = await _joinRoom(roomId, nickname);
    _roomId = roomId;

    notifyListeners();
    return JoinRoomResult(roomId: roomId, me: res.me, token: res.token);
  }

  Future<JoinRoomResponse> _joinRoom(int roomId, String nickname) async {
    return await client.joinRoom(JoinRoomRequest(
      roomId: roomId,
      name: nickname,
    ));
  }
}

class JoinRoomResult {
  final int roomId;
  final Player me;
  final String token;

  JoinRoomResult({required this.roomId, required this.me, required this.token});
}
