import 'dart:collection';
import 'dart:math';

import 'package:csv/csv.dart';
import 'package:csv/csv_settings_autodetection.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart' hide Action;
import 'package:vector_math/vector_math.dart';

import './generated/carcassonne.pb.dart';

String tileKindToName(int kind) {
  const data = [
    'CastleCenterPennant',
    'CastleCenterEntry',
    'CastleCenterSide',
    'CastleEdge',
    'CastleEdgeRoad',
    'CastleSides',
    'CastleSidesEdge',
    'CastleTube',
    'CastleWall',
    'CastleWallCurveLeft',
    'CastleWallCurveRight',
    'CastleWallJunction',
    'CastleWallRoad',
    'Cloister',
    'CloisterRoad',
    'Road',
    'RoadCurve',
    'RoadJunctionLarge',
    'RoadJunctionSmall',
    'CastleCenterEntryPennant',
    'CastleCenterSidePennant',
    'CastleEdgePennant',
    'CastleEdgeRoadPennant',
    'CastleTubePennant',
  ];
  return data[kind];
}

class TileDataRegistry {
  static late HashMap<String, List<List<int>>> _featureIds;
  static late HashMap<String, List<int>> _meepleCenters;

  static Future<void> initialize() async {
    _featureIds = HashMap();
    _meepleCenters = HashMap();

    final csvText = await rootBundle.loadString('assets/misc/tiles.csv');
    List<List<dynamic>> csv = CsvToListConverter(
      csvSettingsDetector: FirstOccurrenceSettingsDetector(
        eols: ['\r\n', '\n'],
        textDelimiters: ['"'],
      ),
      shouldParseNumbers: false,
    ).convert(csvText);
    print('csv = $csv');
    final headRow = csv[0];
    csv.removeAt(0);

    final nameIndex = headRow.indexWhere((name) => name == 'name');
    final featureIdsIndex = headRow.indexWhere((name) => name == 'feature_ids');
    final meepleCentersIndex =
        headRow.indexWhere((name) => name == 'meeple_centers');

    for (final row in csv) {
      print('row = $row');

      final String name = row[nameIndex];
      final String featureId = row[featureIdsIndex];
      final String meepleCenters = row[meepleCentersIndex];

      _featureIds[name] = [];
      final featureIdsForName = featureId.split(',').map(int.parse).toList();
      for (var i = 0; i < 4; i += 1) {
        _featureIds[name]!.add(featureIdsForName.rotated(i * 3));
      }

      _meepleCenters[name] = meepleCenters.split(',').map(int.parse).toList();
    }

    print('_meepleCenters = $_meepleCenters');
  }

  static Point<double> meepleTopLeftFor({
    required Tile tile,
    required MeepleAction action,
    required double tileSize,
    required double meepleSize,
  }) {
    final name = tile.kind.name;
    final meepleCenterIndex =
        action.hasEdge() ? (_meepleCenters[name]![action.edge]) : 12;

    List<double>? params;
    if (meepleCenterIndex == 0) {
      params = [0.25, -0.5, 0, 0];
    } else if (meepleCenterIndex == 1) {
      params = [0.5, -0.5, 0, 0];
    } else if (meepleCenterIndex == 2) {
      params = [0.75, -0.5, 0, 0];
    } else if (meepleCenterIndex == 3) {
      params = [1, -1, 0.25, -0.5];
    } else if (meepleCenterIndex == 4) {
      params = [1, -1, 0.5, -0.5];
    } else if (meepleCenterIndex == 5) {
      params = [1, -1, 0.75, -0.5];
    } else if (meepleCenterIndex == 6) {
      params = [0.75, -0.5, 1, -1];
    } else if (meepleCenterIndex == 7) {
      params = [0.5, -0.5, 1, -1];
    } else if (meepleCenterIndex == 8) {
      params = [0.25, -0.5, 1, -1];
    } else if (meepleCenterIndex == 9) {
      params = [0, 0, 0.75, -0.5];
    } else if (meepleCenterIndex == 10) {
      params = [0, 0, 0.5, -0.5];
    } else if (meepleCenterIndex == 11) {
      params = [0, 0, 0.25, -0.5];
    } else if (meepleCenterIndex == 12) {
      // center
      params = [0.5, -0.5, 0.5, -0.5];
    } else if (meepleCenterIndex == 13) {
      // corners
      params = [0, 0, 0, 0];
    } else if (meepleCenterIndex == 14) {
      params = [1, -1, 0, 0];
    } else if (meepleCenterIndex == 15) {
      params = [1, -1, 1, -1];
    } else if (meepleCenterIndex == 16) {
      params = [0, 0, 1, -1];
    }

    if (params != null) {
      return _calculateMeepleTopLeft(
        tileSize: tileSize,
        meepleSize: meepleSize,
        aX: params[0],
        aY: params[1],
        bX: params[2],
        bY: params[3],
        rotation: tile.rotation.value.toDouble(),
      );
    } else {
      throw RangeError(
        'Meeple center index $meepleCenterIndex is out of valid range',
      );
    }
  }

  /// Calculates the top-left corner coordinates for a meeple within a tile.
  ///
  /// The center point is calculated as `(aX * t + aY * m, bX * t + bY * m)`.
  static Point<double> _calculateMeepleTopLeft({
    required double tileSize,
    required double meepleSize,

    /// Horizontal coefficient to be multiplied by tile size
    required double aX,
    required double aY,
    required double bX,
    required double bY,
    required double rotation,
  }) {
    final halfMeepleSize = Vector2(meepleSize / 2, meepleSize / 2);
    final halfTileSize = Vector2(tileSize / 2, tileSize / 2);
    final center = Vector2(
          aX * tileSize + aY * meepleSize,
          bX * tileSize + bY * meepleSize,
        ) +
        halfMeepleSize;
    final transf = Matrix2.rotation(-pi * 0.5 * rotation);
    final topLeft =
        transf.transform(center - halfTileSize) + halfTileSize - halfMeepleSize;

    return Point(topLeft.x, topLeft.y);
  }
}

extension ListRotate<T> on List<T> {
  List<T> rotated(int dist) {
    if (this.isEmpty) return [];
    final i = dist % this.length;
    return this.sublist(i)..addAll(this.sublist(0, i));
  }
}

extension PlayerMeepleAsset on Player {
  AssetImage meepleAsset() {
    if (this == Player.Red) {
      return AssetImage('assets/misc/MeepleRed.png');
    } else {
      return AssetImage('assets/misc/MeepleBlue.png');
    }
  }
}

extension ActionPlacesMeeple on Action {
  bool get placesMeeple =>
      this.meepleAction.hasEdge() || this.meepleAction.hasCloister();
}
