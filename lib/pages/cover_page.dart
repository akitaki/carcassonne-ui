import 'package:carcassonne/pages/gameplay_page.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:grpc/grpc.dart';
import 'package:git_info/git_info.dart';

import '../models/room_model.dart';
import '../palette.dart';
import '../widgets/list_extensions.dart';

class CoverPage extends StatefulWidget {
  static const String routeName = '/';

  @override
  State<StatefulWidget> createState() => CoverPageState();
}

class CoverPageState extends State<CoverPage> {
  bool _createInProgress = false;

  /// Value of the room id textfield
  String _nickname = '';

  bool get _isNicknameLegal => _nickname.trim().isNotEmpty;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade50,
      body: ChangeNotifierProvider(
        create: (_context) => RoomModel.makeDefault(),
        child: Stack(
          children: [
            Container(
              alignment: Alignment.center,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: AnimationConfiguration.toStaggeredList(
                  childAnimationBuilder: (widget) => SlideAnimation(
                    verticalOffset: 50.0,
                    child: FadeInAnimation(
                      child: widget,
                    ),
                  ),
                  children: _mainColumnChildren(context)
                      .mapPadding(EdgeInsets.symmetric(vertical: 8)),
                  duration: const Duration(milliseconds: 500),
                ),
              ),
            ),
            _versionText()
          ],
        ),
      ),
    );
  }

  Positioned _versionText() {
    return Positioned(
      right: 8,
      bottom: 8,
      child: FutureBuilder(
        future: GitInfo.get(),
        builder: (context, snapshot) {
          final data = snapshot.data as GitInformation?;
          String text = '';
          if (data != null && data.hash != null) {
            text = 'commit ${data.hash!.substring(0, 7)} on ${data.branch}';
          }
          return Text(
            text,
            style: GoogleFonts.robotoMono(
              textStyle: TextStyle(color: Colors.grey),
            ),
          );
        },
      ),
    );
  }

  List<Widget> _mainColumnChildren(BuildContext context) {
    return [
      AnimatedTextKit(
        animatedTexts: [
          TyperAnimatedText(
            'Carcassonne',
            textStyle: GoogleFonts.jimNightshade(
                textStyle: Theme.of(context).textTheme.headline2),
            speed: const Duration(milliseconds: 100),
          ),
        ],
        totalRepeatCount: 1,
      ),
      SizedBox(width: 200, child: Divider()),
      LargeTextField(
        enabled: true,
        onChanged: this._handleNicknameTextFieldChanged,
        hintText: 'Nickname',
      ),
      SizedBox(
        width: 200,
        child: ElevatedButton(
          onPressed: _isNicknameLegal ? this._handleCreatePressed : null,
          child: ProgressButtonText(
            text: 'CREATE NEW ROOM',
            inProgress: this._createInProgress,
            progressColor: Palette.blacks.shade200,
          ),
        ),
      ),
      SizedBox(
        width: 200,
        child: OutlinedButton(
          onPressed: _isNicknameLegal ? this._handleJoinPressed : null,
          child: ProgressButtonText(
            text: 'JOIN ROOM ...',
            inProgress: false,
            progressColor: Palette.blacks.shade200,
          ),
        ),
      ),
      SizedBox(
        width: 200,
        child: OutlinedButton(
          onPressed: _isNicknameLegal ? this._handleSinglePlayerPressed : null,
          child: ProgressButtonText(
            text: 'SINGLE PLAYER',
            inProgress: false,
            progressColor: Palette.blacks.shade200,
          ),
        ),
      ),
    ];
  }

  Future<void> _handleCreatePressed() async {
    this.setState(() {
      _createInProgress = true;
    });
    final roomModel = RoomModel.makeDefault();

    try {
      await Future.delayed(Duration(milliseconds: 500));

      final res = await roomModel.createAndJoinRoom(
        nickname: _nickname,
        singlePlayer: false,
      );
      Navigator.of(context).pushNamed(GameplayPage.routeName,
          arguments: GameplayPageArguments(
            me: res.me,
            token: res.token,
            roomId: res.roomId,
          ));
    } on GrpcError catch (error) {
      _showFailureSnackbar(error.message ?? '$error');
    } finally {
      this.setState(() {
        _createInProgress = false;
      });
    }
  }

  void _handleJoinPressed() {
    Navigator.of(context).pushNamed(
      JoinRoomPage.routeName,
      arguments: JoinRoomPageArguments(this._nickname),
    );
  }

  Future<void> _handleSinglePlayerPressed() async {
    this.setState(() {
      _createInProgress = true;
    });
    final roomModel = RoomModel.makeDefault();

    try {
      await Future.delayed(Duration(milliseconds: 500));

      final res = await roomModel.createAndJoinRoom(
        nickname: _nickname,
        singlePlayer: true,
      );
      Navigator.of(context).pushNamed(GameplayPage.routeName,
          arguments: GameplayPageArguments(
            me: res.me,
            token: res.token,
            roomId: res.roomId,
          ));
    } on GrpcError catch (error) {
      _showFailureSnackbar(error.message ?? '$error');
    } finally {
      this.setState(() {
        _createInProgress = false;
      });
    }
  }

  void _handleNicknameTextFieldChanged(String value) {
    this.setState(() {
      _nickname = value;
    });
  }

  void _showFailureSnackbar(String message) {
    final messenger = ScaffoldMessenger.of(context);
    messenger.removeCurrentSnackBar();
    messenger.showSnackBar(
      SnackBar(
        duration: Duration(days: 365),
        width: 280,
        behavior: SnackBarBehavior.floating,
        content: Text(message),
      ),
    );
  }
}

class JoinRoomPage extends StatefulWidget {
  static const routeName = '/join';

  final JoinRoomPageArguments args;

  JoinRoomPage(this.args);

  @override
  State<StatefulWidget> createState() => JoinRoomPageState();
}

class JoinRoomPageArguments {
  final String nickname;
  JoinRoomPageArguments(this.nickname);
}

class JoinRoomPageState extends State<JoinRoomPage> {
  bool _joinInProgress = false;
  String _roomId = '';

  bool get _isRoomIdLegal => int.tryParse(_roomId) != null;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade50,
      appBar: AppBar(
        title: Text('Join existing room'),
        backgroundColor: Colors.transparent,
        foregroundColor: Colors.grey.shade900,
        shadowColor: Colors.transparent,
        backwardsCompatibility: false,
      ),
      body: ChangeNotifierProvider(
        create: (_context) => RoomModel.makeDefault(),
        child: Container(
          alignment: Alignment.center,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: AnimationConfiguration.toStaggeredList(
              childAnimationBuilder: (widget) => SlideAnimation(
                horizontalOffset: 50.0,
                child: FadeInAnimation(
                  child: widget,
                ),
              ),
              children: _mainColumnChildren(context)
                  .mapPadding(EdgeInsets.symmetric(vertical: 8)),
              duration: const Duration(milliseconds: 500),
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _mainColumnChildren(context) {
    return [
      LargeTextField(
        enabled: true,
        onChanged: this._handleRoomIdTextFieldChanged,
        hintText: 'Room ID',
      ),
      SizedBox(
        width: 200,
        child: ElevatedButton(
          onPressed: _isRoomIdLegal ? this._handleJoinPressed : null,
          child: ProgressButtonText(
            text: 'JOIN',
            inProgress: this._joinInProgress,
            progressColor: Palette.blacks.shade200,
          ),
        ),
      ),
    ];
  }

  void _handleRoomIdTextFieldChanged(String value) {
    setState(() {
      this._roomId = value;
    });
  }

  Future<void> _handleJoinPressed() async {
    this.setState(() {
      _joinInProgress = true;
    });
    final roomModel = RoomModel.makeDefault();

    try {
      await Future.delayed(Duration(milliseconds: 500));

      final res = await roomModel.joinRoom(
        int.parse(this._roomId),
        widget.args.nickname,
      );
      Navigator.of(context).pushNamed(GameplayPage.routeName,
          arguments: GameplayPageArguments(
            me: res.me,
            token: res.token,
            roomId: res.roomId,
          ));
    } on GrpcError catch (error) {
      _showFailureSnackbar(error.message ?? '$error');
    } finally {
      this.setState(() {
        _joinInProgress = false;
      });
    }
  }

  void _showFailureSnackbar(String message) {
    final messenger = ScaffoldMessenger.of(context);
    messenger.removeCurrentSnackBar();
    messenger.showSnackBar(
      SnackBar(
        duration: Duration(days: 365),
        width: 280,
        behavior: SnackBarBehavior.floating,
        content: Text(message),
      ),
    );
  }
}

/// Button text widget that can show progress indicator.
class ProgressButtonText extends StatelessWidget {
  final String text;
  final bool inProgress;
  final Color progressColor;

  ProgressButtonText({
    required this.text,
    required this.inProgress,
    required this.progressColor,
  });

  @override
  Widget build(BuildContext context) {
    // throw UnimplementedError();
    return Stack(
      alignment: Alignment.center,
      children: [
        if (this.inProgress)
          SizedBox(
            width: 14,
            height: 14,
            child: CircularProgressIndicator(
              strokeWidth: 2,
              valueColor: AlwaysStoppedAnimation<Color>(this.progressColor),
            ),
          ),
        Visibility(
          child: Text(this.text),
          maintainSize: true,
          maintainAnimation: true,
          maintainState: true,
          visible: this.inProgress == false,
        ),
      ],
    );
  }
}

class LargeTextField extends StatelessWidget {
  final bool enabled;
  final ValueChanged<String>? onChanged;
  final String? hintText;
  LargeTextField({
    required this.enabled,
    this.onChanged,
    this.hintText,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 250,
      child: TextField(
        style: GoogleFonts.firaMono(
          textStyle: TextStyle(
            color: Palette.blacks.shade400,
            fontSize: 24.0,
            letterSpacing: 2.0,
          ),
        ),
        textAlign: TextAlign.center,
        autofocus: true,
        onChanged: this.onChanged,
        enabled: enabled,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(vertical: 16.0),
          hintText: this.hintText,
          hintStyle: TextStyle(
              fontSize: 24.0,
              letterSpacing: 1.0,
              color: Colors.grey.withOpacity(0.4)),
          filled: true,
        ),
      ),
    );
  }
}
