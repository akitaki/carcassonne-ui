import 'package:flutter/material.dart';
import 'package:animations/animations.dart';

/// Global default theme data
class Palette {
  static const MaterialColor blacks = const MaterialColor(
    0xff404033, // 0% comes in here, this will be color picked if no shade is selected when defining a Color property which doesn’t require a swatch.
    const <int, Color>{
      50: const Color(0xffff404033), //10%
      100: const Color(0xffff535347), //20%
      200: const Color(0xffff66665c), //30%
      300: const Color(0xffff797970), //40%
      400: const Color(0xffff8c8c85), //50%
      500: const Color(0xffffa0a099), //60%
      600: const Color(0xffffb3b3ad), //70%
      700: const Color(0xffffc6c6c2), //80%
      800: const Color(0xffffd9d9d6), //90%
      900: const Color(0xffffececeb), //100%
    },
  );

  static const MaterialColor reds = MaterialColor(
    0xFFC91F37,
    {
      50: Color(0xFFFDF4F5),
      100: Color(0xFFFAE9EB),
      200: Color(0xFFF2C7CD),
      300: Color(0xFFE9A4AD),
      400: Color(0xFFDA6373),
      500: Color(0xFFC91F37),
      600: Color(0xFFB31C31),
      700: Color(0xFF791321),
      800: Color(0xFF5B0E19),
      900: Color(0xFF3B0910),
    },
  );

  static const MaterialColor blues = MaterialColor(
    0xFF4D646C,
    {
      50: Color(0xFFF7F8F8),
      100: Color(0xFFEEF0F1),
      200: Color(0xFFD3D9DB),
      300: Color(0xFFB7C0C3),
      400: Color(0xFF839399),
      500: Color(0xFF4D646C),
      600: Color(0xFF455961),
      700: Color(0xFF2F3C41),
      800: Color(0xFF232D31),
      900: Color(0xFF171D20),
    },
  );

  static ThemeData defaultThemeData = ThemeData(
    primarySwatch: Palette.blacks,
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 20),
      ),
    ),
    outlinedButtonTheme: OutlinedButtonThemeData(
      style: OutlinedButton.styleFrom(
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 20),
      ),
    ),
    pageTransitionsTheme: PageTransitionsTheme(
      builders: {
        TargetPlatform.linux: SharedAxisPageTransitionsBuilder(
          transitionType: SharedAxisTransitionType.horizontal,
        ),
        TargetPlatform.android: SharedAxisPageTransitionsBuilder(
          transitionType: SharedAxisTransitionType.horizontal,
        ),
        TargetPlatform.iOS: SharedAxisPageTransitionsBuilder(
          transitionType: SharedAxisTransitionType.horizontal,
        ),
      },
    ),
  );
}
